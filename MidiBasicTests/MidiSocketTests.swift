//
//  MidiSocketTests.swift
//  MidiBasicTests
//
//  Created by Claude on 31/12/2017.
//  Copyright © 2017 Claude. All rights reserved.
//

import XCTest
@testable import MidiBasic

class MidiSocketTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testInitForMidiSocket() {
        let myMidi = MidiSocket(midiDeviceName: "Test")
        XCTAssertNotEqual(myMidi.client,0)
        XCTAssertNotEqual(myMidi.outPort,0)
        XCTAssertNotEqual(myMidi.inPort,0)
        
    }
    
    func testGetDestinationList(){
        let myMidi = MidiSocket(midiDeviceName: "Test")
        let destinations = myMidi.getDestinations()
        XCTAssertEqual(destinations.count, 5)
        XCTAssertEqual(destinations[0],"Réseau Session Mac 1")
    }
    
    func testSetDestination() {
        let myMidi = MidiSocket(midiDeviceName: "Test")
        myMidi.setActiveDestination(destinationNumber: 1)
        let destinationNumber = myMidi.getActiveDestinationNumber()
        XCTAssertNotEqual(myMidi.activeDestination,0)
        XCTAssertEqual(destinationNumber, 1)
    }
    
    func testSendSomeDataTodesination(){
        let myMidi = MidiSocket(midiDeviceName: "Test")
        myMidi.setActiveDestination(destinationNumber: 1)
        
        myMidi.sendBytes([0x90,0x3C,100])
        sleep(1)
        myMidi.sendBytes([0x80,0x3C,100])
    }
    
    func testGetSourceList(){
        let myMidi = MidiSocket(midiDeviceName: "Test")
        let sources = myMidi.getSources()
        XCTAssertEqual(sources.count, 4)
        XCTAssertEqual(sources[0],"Réseau Session Mac 1")
    }
    
    func testSetSource() {
        let myMidi = MidiSocket(midiDeviceName: "Test")
        myMidi.setActiveSource(sourceNumber: 1)
        let sourceNumber = myMidi.getActiveSourceNumber()
        XCTAssertNotEqual(myMidi.activeSource,0)
        XCTAssertEqual(sourceNumber, 1)
    }
   
}

