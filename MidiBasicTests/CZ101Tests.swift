//
//  CZ101Tests.swift
//  MidiBasicTests
//
//  Created by Claude on 10/01/2018.
//  Copyright © 2018 Claude. All rights reserved.
//

import XCTest
@testable import MidiBasic

class CZ101Tests: XCTestCase {

    override func setUp() {
        super.setUp()
        
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSetOctave(){
        let cz = CZ101()
        for oct in -1...1{
            cz.setOctave(oct)
            let result = cz.getOctave()
            XCTAssertEqual(oct,result)
        }
    }
    
    func testSetLineSelect(){
        let cz = CZ101()
        for line in 0...3{
            cz.setLineSelect(line)
            let result = cz.getLineSelect()
            XCTAssertEqual(line,result)
        }
    }
    
    func testLineXOctave(){
        let cz = CZ101()
        for oct in -1...1{
            cz.setOctave(oct)
            for line in 0...3{
                cz.setLineSelect(line)
                let lineResult = cz.getLineSelect()
                let octResult = cz.getOctave()
                XCTAssertEqual(line,lineResult)
                XCTAssertEqual(oct,octResult)
            }
        }
    }
    
    func testDetuneFine(){
        let cz = CZ101()
        for detune in 0...60{
            cz.setDetuneFine(detune)
            let result = cz.getDetuneFine()
            XCTAssertEqual(detune,result)
        }
    }
    
    func testDetuneOctave() {
        let cz = CZ101()
        for octave in 0...3 {
            cz.setDetuneOctave(octave)
            let resultOctave = cz.getDetuneOctave()
            XCTAssertEqual(octave,resultOctave)
        }
    }
    
    func testDetuneNote() {
        let cz = CZ101()
        for note in 0...11 {
            cz.setDetuneNote(note)
            let resultNote = cz.getDetuneNote()
            XCTAssertEqual(note,resultNote)
        }
    }
    
    func testDetuneOctaveXNote() {
        let cz = CZ101()
        for octave in 0...3 {
            for note in 0...11 {
                cz.setDetuneNote(note)
                cz.setDetuneOctave(octave)
                let resultNote = cz.getDetuneNote()
                let resultOctave = cz.getDetuneOctave()
                XCTAssertEqual(note,resultNote)
                XCTAssertEqual(octave,resultOctave)
            }
        }
    }
    
    func testVibratoDelay() {
        let cz = CZ101()
        for vibratoDelay in 0...99 {
            cz.setVibratoDelay(vibratoDelay)
            let resultVibratoDelay = cz.getVibratoDelay()
            XCTAssertEqual(vibratoDelay,resultVibratoDelay)
        }
    }
    
    func testVibratoRate() {
        let cz = CZ101()
        for vibratoRate in 0...99 {
            cz.setVibratoRate(vibratoRate)
            let resultvibratoRate = cz.getVibratoRate()
            XCTAssertEqual(vibratoRate,resultvibratoRate)
        }
    }
    
    func testVibratoDepth() {
        let cz = CZ101()
        for vibratoDepth in 0...99 {
            cz.setVibratoDepth(vibratoDepth)
            let resultVibratoDepth = cz.getVibratoDepth()
            XCTAssertEqual(vibratoDepth,resultVibratoDepth)
        }
    }
    
    func testDCO1FirstWF(){
        let cz = CZ101()
        for wf in 1...8 {
            cz.setDC01FirstWF(wf)
            let resultWf = cz.getDC01FirstWF()
            XCTAssertEqual(wf,resultWf)
        }
    }
    
    func testDCO1SecondWF(){
        let cz = CZ101()
        for wf in 0...8 {
            cz.setDC01SecondWF(wf)
            let resultWf = cz.getDC01SecondWF()
            XCTAssertEqual(wf,resultWf)
        }
    }
    
    func testModulation() {
        let cz = CZ101()
        for mod in 0...2 {
            cz.setModulation(mod)
            let resultMod = cz.getModulation()
            XCTAssertEqual(mod,resultMod)
        }
        
    }
    
    func testDCO1WfXModulation (){
        let cz = CZ101()
        for wf1 in 1...8 {
            for wf2 in 0...5 {
                for mod in 0...2 {
                 
                    cz.setModulation(mod)
                    cz.setDC01FirstWF(wf1)
                    cz.setDC01SecondWF(wf2)
                   
                    let resultMod = cz.getModulation()
                    let resultWf1 = cz.getDC01FirstWF()
                  
                    let resultWf2 = cz.getDC01SecondWF()
                  
                    XCTAssertEqual(mod,resultMod)
                    XCTAssertEqual(wf1,resultWf1)
                   
                    XCTAssertEqual(wf2,resultWf2)
                    
                }
            }
        }
    }
    
    func testDCA1KeyFollow() {
        let cz = CZ101()
        for keyFollow in 0...9 {
            cz.setDCA1KeyFollow(keyFollow)
            let resultKeyFollow = cz.getDCA1KeyFollow()
            XCTAssertEqual(keyFollow,resultKeyFollow)
        }
    }
    
    func testDCW1KeyFollow() {
        let cz = CZ101()
        for keyFollow in 0...9 {
            cz.setDCW1KeyFollow(keyFollow)
            let resultKeyFollow = cz.getDCW1KeyFollow()
            XCTAssertEqual(keyFollow,resultKeyFollow)
        }
    }
    
    func testDCA1EndStep() {
        let cz = CZ101()
        for step in 1...8 {
            cz.setDCA1EndStep(step)
            let resultStep = cz.getDCA1EndStep()
            XCTAssertEqual(step,resultStep)
        }
    }
    
    func testDCA1SustainStep() {
        let cz = CZ101()
        for step in 1...8 {
            cz.setDCA1SustainStep(step)
            let resultStep = cz.getDCA1SustainStep()
            XCTAssertEqual(step,resultStep)
        }
    }
    
    
    
}
