//
//  CZ101.swift
//  MidiBasic
//
//  Created by Claude on 04/01/2018.
//  Copyright © 2018 Claude. All rights reserved.
//

import Foundation


func extractDataFromSysEx(_ input:[UInt8]) -> [UInt8]{
    var result = [UInt8]()
    for i in 0...127 {
        result.append(input[i*2+6]+input[i*2+7]*16)
    }
    return result
}

func binaryUInt8ToString(_ num:UInt8)->String {
    var result = ""
    var buff = num
    for c in 0...7 {
        if (c == 4) {result = " " + result}
        if (buff % 2 == 1){
            result = "1" + result
        } else {
            result = "0" + result
        }
        buff = buff/2
    }
    return result
}

let initPreset:[UInt8] = [
    0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x07, 0x77, 0xFF, 0xBC, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x3C,
    0x00, 0x3C, 0x00, 0xBC, 0x00, 0x07, 0x7F, 0xFF, 0xC4, 0x00, 0x44, 0x00, 0x44, 0x00, 0x44, 0x00,
    0x44, 0x00, 0x44, 0x00, 0xC4, 0x00, 0x07, 0x40, 0x80, 0x40, 0x00, 0x40, 0x00, 0x40, 0x00, 0x40,
    0x00, 0x40, 0x00, 0x40, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x77, 0xFF,
    0xBC, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0x3C, 0x00, 0xBC, 0x00, 0x07, 0x7F,
    0xFF, 0xC4, 0x00, 0x44, 0x00, 0x44, 0x00, 0x44, 0x00, 0x44, 0x00, 0x44, 0x00, 0xC4, 0x00, 0x07,
    0x40, 0x80, 0x40, 0x00, 0x40, 0x00, 0x40, 0x00, 0x40, 0x00, 0x40, 0x00, 0x40, 0x00, 0xC0, 0x00
]

func requestProgramMessage(programNumber: UInt8, channel: UInt8) -> [UInt8]{
    var midi = [UInt8]()
    //myMidi.sendBytes(bytesToSend: [0xF0,  0x44,0x00,0x00,  0x70,   0x10,   0x20,       0x70,0x31])
    //                               sysex  casio            chanel  dump    prestet        mi
    midi.append(contentsOf: [0xF0,0x44,0x00,0x00]) //sysex  + Casio Identifier
    midi.append(0x70 + channel - 1)  // 1..15
    midi.append(0x10) //dump
    midi.append(programNumber)
    midi.append(0x70 + channel - 1)
    midi.append(0x31)
    return midi
}

enum EnvName {
    //case DC01
    case DCW1
    case DCA1
    //case DC02
    //case DCW2
    //case DCA2
    
}

public class CZ101 {
    
    var data:[UInt8] = initPreset
    
    func setDataFromSysEx(_ input:[UInt8]){
        data = []
        for i in 0...127 {
            data.append(input[i*2+6]+input[i*2+7]*16)
        }
    }
    
    func getSysExFromDataFor(programNumber: UInt8, channel: UInt8) -> [UInt8]{
        var message:[UInt8]=[]
        message.append(contentsOf: [0xF0,0x44,0x00,0x00]) //sysex  + Casio Identifier
        message.append(0x70 + channel - 1)  // 1..15
        message.append(0x20) //set
        message.append(programNumber)
        // 5F --> 0F 05
        for byte in data {
            let low:UInt8 = byte & 0x0F
            var hight:UInt8 = byte & 0xF0
            hight = byte >> 4
            message.append(low)
            message.append(hight)
        }
        message.append(0xF7)
        return message
    }
    
// MARK:- Get Functions
    
    func getOctave()-> Int {
        let pflag = data[0x00]
        
        switch (pflag & 0x0C){
        case 0x00 :
            return 0
        case 0x04 :
            return 1
        case 0x08 :
            return -1
        default :
            return 0
        }
    }
    
    func getLineSelect()-> Int {
        let pflag = data[0x00]
        return Int(pflag & 0x03)
    }
    
    func getLineSelectString()->String{
        let i = getLineSelect()
        switch i {
        case 0:
            return "1"
        case 1:
            return "2"
        case 2:
            return "1 + 1'"
        case 3:
            return "1 + 2'"
        default:
            return "?"
        }
    }
    
    func getDetunePol()->Int{
        return Int(data[0x01])
        
    }
    
    func getDetunePolString()->String{
        if (getDetunePol() == 0){
            return "+"
        } else {
            return "-"
        }
    }
    
    func getDetuneFine() -> Int {
        //pdetl
        var pdetl = data[0x02]
        pdetl = pdetl >> 2
        return Int(pdetl - pdetl / 16)
    }
    
    
    func getDetuneOctave() -> Int {
        //pdeth
        let pdeth = data[0x03]
        return Int(pdeth/12)
    }
    
    func getDetuneNote() -> Int {
        //pdeth
        let pdeth = data[0x03]
        return Int(pdeth - (pdeth/12)*12)
    }
    
    func getVibratoWave() -> Int{
        let pvk = data[0x04]
        switch pvk {
        case 0x08:
            return 1
        case 0x04:
            return 2
        case 0x20:
            return 3
        case 0x02:
            return 4
        default:
            return 0
        }
    }
    
    func getVibratoWaveString() -> String{
        switch getVibratoWave() {
        case 1:
            return "Tri"
        case 2:
            return "UpRamp"
        case 3:
            return "DownRamp"
        case 4:
            return "Square"
        default:
            return "?"
        }
    }
    
    func getVibratoDelay() -> Int {
        let b1 = data[0x05]
        var b2:UInt8
        var b3:UInt8
        
        if (b1<32) {
            b2 = b1
            b3 = 0
        } else if (b1<48) {
            b2 = b1*2 - 31
            b3 = 0
        } else if (b1<64) {
            b2 = (b1-48)*4 + 67
            b3 = 0
        }  else if (b1<80) {
            b2 = (b1-64)*8 + 135
            b3 = 0
        }  else if (b1<96) {
            b2 = (b1-80)*16 + 15
            b3 = 1
        } else {
            b2 = (b1-96)*32 + 31
            b3 = 2
        }
        if (b2 != data[0x06]){
            print("ERROR on byte 0x06")
        }
        if (b3 != data[0x07]){
            print("ERROR on byte 0x07")
        }
        return Int(b1)
    }
    
    let codeFor0x09:[UInt8] = [
        0x20,0x40,0x60,0x80,0xA0,0xC0,0xE0,0x00,0x20,0x40,0x60,0x80,0xA0,0xC0,0xE0,0x00,
        0x20,0x40,0x60,0x80,0xA0,0xC0,0xE0,0x00,0x20,0x40,0x60,0x80,0xA0,0xC0,0xE0,0x20,
        0x60,0xA0,0xE0,0x20,0x60,0xA0,0xE0,0x20,0x60,0xA0,0xE0,0x20,0x60,0xA0,0xE0,0x60,
        0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0x60,0xE0,0xE0,
        0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,
        0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,0xE0,
        0xE0,0xE0,0xE0,0xE0]
    
    let codeFor0x0A:[UInt8] = [
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x02,
        0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x03,0x04,
        0x04,0x04,0x04,0x05,0x05,0x05,0x05,0x06,0x06,0x06,0x06,0x07,0x07,0x07,0x07,0x08,
        0x08,0x09,0x09,0x0A,0x0A,0x0B,0x0B,0x0C,0x0C,0x0D,0x0D,0x0E,0x0E,0x0F,0x0F,0x10,
        0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x21,
        0x23,0x25,0x27,0x29,0x2B,0x2D,0x2F,0x31,0x33,0x35,0x37,0x39,0x3B,0x3D,0x3F,0x43,
        0x47,0x4B,0x4F,0x53]
    
    
    func getVibratoRate() -> Int {
        let rate = Int(data[0x08])
        let b2 = codeFor0x09[rate]
        let b3 = codeFor0x0A[rate]
        if (b2 != data[0x09]){
            print("ERROR on byte 0x09")
        }
        if (b3 != data[0x0A]){
            print("ERROR on byte 0x0A")
        }
        return rate
    }
    
    func getVibratoDepth() -> Int {
        let b1 = data[0x0B]
        
        var b2:UInt8
        var b3:UInt8
        
        if (b1<31) {
            b2 = b1+1
            b3 = 0
        } else if (b1<47) {
            b2 = (b1-31)*2 + 33
            b3 = 0
        } else if (b1<63) {
            b2 = (b1-47)*4 + 67
            b3 = 0
        }  else if (b1<79) {
            b2 = (b1-63)*8 + 135
            b3 = 0
        }  else if (b1<95) {
            b2 = (b1-79)*16 + 15
            b3 = 1
        } else if (b1<99) {
            b2 = (b1-95)*32 + 31
            b3 = 2
        } else {
            b2 = 0
            b3 = 3
        }
        if (b2 != data[0x0C]){
            print("ERROR on byte 0x0C")
        }
        if (b3 != data[0x0D]){
            print("ERROR on byte 0x0D")
        }
        return Int(b1)
    }
    
    private func getResonanceWF(_ rwf:UInt8) -> Int{
        switch Int(rwf) {
        case 0b01:
            return 6
        case 0b10:
            return 7
        case 0b11:
            return 8
        default:
            print("ERROR in ResonanceWF")
            return 0
        }
    }
    
    func getDC01FirstWF()-> Int {
        let b1 = data[0x0E]
        let b2 = data[0x0F]
        
        let wf1 = (b1 & 0xE0) >> 5
        let wf2 = (b2 & 0xC0) >> 6
        
        switch wf1 {
        case 0b000:
            return 1
        case 0b001:
            return 2
        case 0b010:
            return 3
        case 0b100:
            return 4
        case 0b101:
            return 5
        case 0b110:
            return getResonanceWF(wf2)
        default:
            print("ERROR on byte 0xE0")
            return 0
        }
    }
    
    func getDC01SecondWF()-> Int {
        let b1 = data[0x0E]
        let b2 = data[0x0F]
       
        let wf1 = (b1 & 0x1E) >> 1
        let wf2 = (b2 & 0xC0) >> 6
        
        switch wf1 {
        case 0b0000:
            return 0
        case 0b0001:
            return 1
        case 0b0011:
            return 2
        case 0b0101:
            return 3
        case 0b1001:
            return 4
        case 0b1011:
            return 5
        case 0b1101:
            return getResonanceWF(wf2)
        default:
            print("ERROR on byte 0xE0")
            return 0
        }
    }
    
    func getModulation() -> Int {
        let b = data[0x0F]
        let mod = (b & 0x30) >> 4
        return Int(mod)
    }
    
    func getModulationString() -> String {
        switch getModulation() {
        case 0:
            return "OFF"
        case 1:
            return "NOISE"
        case 2:
            return "RING"
        default:
            return "?"
        }
    }
    
    let byte0x0F_Value:[UInt8] = [0x00,0x08,0x11,0x1A,0x24,0x2F,0x3A,0x45,0x52,0x5F]
    
    func getDCA1KeyFollow() -> Int {
        let b1 = data[0x10]
        let b2 = data[0x11]
        
        let b2Check = byte0x0F_Value[Int(b1)]
        if (b2 != b2Check){
            print("ERROR on byte 0x11")
        }
        return Int(b1)
    }
    
    let byte0x13_Value:[UInt8] = [0x00,0x1F,0x2C,0x39,0x46,0x53,0x60,0x6E,0x92,0xFF]
    
    func getDCW1KeyFollow() -> Int {
        let b1 = data[0x12]
        let b2 = data[0x13]
        
        let b2Check = byte0x13_Value[Int(b1)]
        if (b2 != b2Check){
            print("ERROR on byte 0x13")
        }
        return Int(b1)
    }
    
    func getDCA1EndStep() -> Int {
        let b = data[0x14] + 1
        return Int(b)
    }
    
    func getDCA1SustainStep() -> Int {
        var sustain = 0
        for step in 1...8 {
            let adr = 0x16 + (step-1)*2
            let sustainByte = data[adr] & 0x80
            if (sustainByte == 0x80){
                sustain = step
            }
        }
        return sustain
    }
    
    func getDCA1LevelForStep(_ step:Int) -> Int {
        let stepAdr = 0x16 + (step-1)*2
        let d = data[stepAdr] & 0b01111111
        
        if (d == 0 ){
            return 0
        } else if (d == 0x7F) {
            return 99
        } else {
            return Int(d-0x1C)
            
        }
    }
    
   
    
    
    func levelGoesDownAtStep(_ step :Int) -> Bool{
        if (step == 1) {
            return false
        } else {
            let fromLevel = getDCA1LevelForStep(step-1)
            let toLevel = getDCA1LevelForStep(step)
            if (fromLevel <= toLevel){
                return false
            } else {
                return true
            }
        }
    }
    
    func levelGoesDown(step: Int,env: EnvName) -> Bool{
        
        if (step == 1) {
            return false
        }
        var fromLevel = 0
        var toLevel = 0
        
        switch (env) {
        case EnvName.DCA1:
            fromLevel = getDCA1LevelForStep(step-1)
            toLevel = getDCA1LevelForStep(step)
            break
        case EnvName.DCW1:
            fromLevel = getDCW1LevelForStep(step-1)
            toLevel = getDCW1LevelForStep(step)
            break
        }
        
        if (fromLevel <= toLevel){
            return false
        } else {
            return true
        }
        
    }
    
    func getDCA1RateForStep(_ step:Int) -> Int {
        let stepAdr = 0x15 + (step-1)*2
        var d = Double(data[stepAdr])
       
        if levelGoesDown(step: step, env: EnvName.DCA1){
            d = d-0x80
        }
        
        if (d == 0 ){
            return 0
        } else if (d == 0x77) {
            return 99
        } else {
            return Int(floor(d * 99)/0x77 + 1)
        }
    }
    
    func getDCW1EndStep() -> Int {
        let b = data[0x25] + 1
        return Int(b)
    }
    
    func getDCW1SustainStep() -> Int {
        var sustain = 0
        for step in 1...8 {
            let adr = 0x27 + (step-1)*2
            let sustainByte = data[adr] & 0x80
            if (sustainByte == 0x80){
                sustain = step
            }
        }
        return sustain
    }
    
    func getDCW1LevelForStep(_ step:Int) -> Int {
        let stepAdr = 0x27 + (step-1)*2
        let d = Double(data[stepAdr] & 0b01111111)
        
        if (d == 0 ){
            return 0
        } else if (d == 0x7F) {
            return 99
        } else {
            return Int(floor(d * 99)/0x7F + 1)
        }
    }
    
    func getDCW1RateForStep(_ step:Int) -> Int {
        let stepAdr = 0x26 + (step-1)*2
        var d = Double(data[stepAdr])
        
        if levelGoesDown(step: step, env: EnvName.DCW1){
            d = d-0x80
        }
        
       
        
        if (d == 8 ){
            return 0
        } else if (d == 0x7F) {
            return 99
        } else {
            return Int(floor((d-8) * 99)/119 + 1)
           
        }
    }
    
    func getDCO1EndStep() -> Int {
        let b = data[0x36] + 1
        return Int(b)
    }
    
    func getDCO1SustainStep() -> Int {
        var sustain = 0
        for step in 1...8 {
            let adr = 0x38 + (step-1)*2
            let sustainByte = data[adr] & 0x80
            if (sustainByte == 0x80){
                sustain = step
            }
        }
        return sustain
    }
    
    func getDCO1LevelForStep(_ step:Int) -> Int {
        return 0
    }
    
    func getDCO1RateForStep(_ step:Int) -> Int {
        return 0
    }
    
    // MARK:- Set Functions
    
    func setOctave(_ octave: Int){
        switch (octave){
        case 0 :
            data[0x00] = (data[0x00] & 0b11110011) + 0x00
            break
        case 1 :
            data[0x00] = (data[0x00] & 0b11110011) + 0x04
            break
        case -1 :
            data[0x00] = (data[0x00] & 0b11110011) + 0x08
            break
        default :
            data[0x00] = (data[0x00] & 0b11110011) + 0x00
        }
    }
    
    func setLineSelect(_ line: Int) {
        if 0...3 ~= line {
            data[0x00] = (data[0x00] & 0b11111100) + UInt8(line)
        }
    }
    
    func setDetuneFine(_ detune: Int) {
        if detune>=0 && detune<60 {
            let detuneValue = detune + detune/15
            data[0x02] = UInt8(detuneValue)
            data[0x02] = data[0x02] << 2
        }
        if detune==60 {
            data[0x02] = (0x3F<<2)
        }
    }
    
    func setDetuneOctave(_ detuneOctave: Int){
        if 0...3 ~= detuneOctave {
            let note = getDetuneNote()
            data[0x03] = UInt8(detuneOctave*12 + note)
        }
    }
    
    func setDetuneNote(_ detuneNote: Int) {
        if 0...11 ~= detuneNote {
            let octave = getDetuneOctave()
            data[0x03] = UInt8(detuneNote + octave*12)
        }
    }
    
    func setVibratoWave(_ wave : Int){
        let pvk = data[0x04]
        switch pvk {
        case 1:
            data[0x04] = 0x08
            break
        case 2:
            data[0x04] = 0x04
            break
        case 3:
            data[0x04] = 0x20
            break
        case 4:
            data[0x04] = 0x02
            break
        default:
            data[0x04] = 0x00
        }
    }
    
    
    func setVibratoDelay(_ delay : Int) {
        let b1 = UInt8(delay)
        var b2:UInt8
        var b3:UInt8
        
        if (b1<32) {
            b2 = b1
            b3 = 0
        } else if (b1<48) {
            b2 = b1*2 - 31
            b3 = 0
        } else if (b1<64) {
            b2 = (b1-48)*4 + 67
            b3 = 0
        }  else if (b1<80) {
            b2 = (b1-64)*8 + 135
            b3 = 0
        }  else if (b1<96) {
            b2 = (b1-80)*16 + 15
            b3 = 1
        } else {
            b2 = (b1-96)*32 + 31
            b3 = 2
        }
        data[0x05] = b1
        data[0x06] = b2
        data[0x07] = b3
    }
 
    
    func setVibratoRate(_ rate: Int) {
        if 0...99 ~= rate {
            data[0x08] = UInt8(rate)
            data[0x09] = codeFor0x09[rate]
            data[0x0A] = codeFor0x0A[rate]
        }
    }
    
    func setVibratoDepth(_ depth : Int) {
        if 0...99 ~= depth {
            let b1 = UInt8(depth)
            var b2:UInt8
            var b3:UInt8
            if (b1<31) {
                b2 = b1+1
                b3 = 0
            } else if (b1<47) {
                b2 = (b1-31)*2 + 33
                b3 = 0
            } else if (b1<63) {
                b2 = (b1-47)*4 + 67
                b3 = 0
            }  else if (b1<79) {
                b2 = (b1-63)*8 + 135
                b3 = 0
            }  else if (b1<95) {
                b2 = (b1-79)*16 + 15
                b3 = 1
            } else if (b1<99) {
                b2 = (b1-95)*32 + 31
                b3 = 2
            } else {
                b2 = 0
                b3 = 3
            }
            data[0x0B] = b1
            data[0x0C] = b2
            data[0x0D] = b3
        }
    }
    
    private func getResonanceByte(_ rwf:Int) -> UInt8{
        switch Int(rwf) {
            
        case 6:
            return 0b01
        case 7:
            return 0b10
        case 8:
            return 0b11
        default:
            return 0
        }
    }
    
    
    func setDC01FirstWF(_ wf: Int) {
        var wf1 :UInt8 = 0
        var wf2 :UInt8 = 0
        switch wf {
        case 1:
            wf1 = 0b000
            break
        case 2:
            wf1 = 0b001
            break
        case 3:
            wf1 = 0b010
            break
        case 4:
            wf1 = 0b100
            break
        case 5:
            wf1 = 0b101
            break
        case 6...8:
            wf1 = 0b110
            wf2 = getResonanceByte(wf)
            data[0x0F] = (data[0x0F] & 0b00111111) + (wf2 << 6)
            break
        default:
            print("DCO1 first WF note in range 1..8")
        }
        data[0x0E] = (data[0x0E] & 0b00011111) + (wf1 << 5)
        
    }
    
    
    func setDC01SecondWF(_ wf: Int) {
        var wf1 :UInt8 = 0
        var wf2 :UInt8 = 0
        switch wf {
            
        case 0:
            wf1 = 0b0000
            break
        case 1:
            wf1 = 0b0001
            break
        case 2:
            wf1 = 0b0011
            break
        case 3:
            wf1 = 0b0101
            break
        case 4:
            wf1 = 0b1001
            break
        case 5:
            wf1 = 0b1011
            break
        case 6...8:
            wf1 = 0b1101
            wf2 = getResonanceByte(wf)
            data[0x0F] = (data[0x0F] & 0b00111111) + (wf2 << 6)
            break
        default:
            print("DCO1 second WF note in range 0..8")
        }
        data[0x0E] = (data[0x0E] & 0b11100001) + (wf1 << 1)
        
    }
    
    
    func setModulation(_ mod: Int) {
        if 0...2 ~= mod {
            let b = UInt8(mod)
            data[0x0F] = (data[0x0F] & 0b11001111) + (b << 4)
        }
    }
    
    func setDCA1KeyFollow(_ keyFollow: Int){
        if 0...9 ~= keyFollow {
            data[0x10] = UInt8(keyFollow)
            data[0x11] = byte0x0F_Value[keyFollow]
        }
    }
    
    func setDCW1KeyFollow(_ keyFollow: Int){
        if 0...9 ~= keyFollow {
            data[0x12] = UInt8(keyFollow)
            data[0x13] = byte0x13_Value[keyFollow]
        }
    }
    
    func setDCA1EndStep(_ step: Int) {
        if 1...8 ~= step {
            data[0x14] = UInt8(step - 1)
        }
    }
    
    func setDCA1SustainStep(_ stepNb: Int){
        for step in 1...8 {
            let adr = 0x16 + (step-1)*2
            if (step == stepNb){
                data[adr] = (data[adr] & 0b01111111) + 0x80
            } else {
                data[adr] = (data[adr] & 0b01111111)
            }
        }
    }
    
    /*
     
    
    
    func getDCA1SustainStep() -> Int {
        var sustain = 0
        for step in 1...8 {
            let adr = 0x16 + (step-1)*2
            let sustainByte = data[adr] & 0x80
            if (sustainByte == 0x80){
                sustain = step
            }
        }
        return sustain
    }
    
    func getDCA1LevelForStep(_ step:Int) -> Int {
        let stepAdr = 0x16 + (step-1)*2
        let d = data[stepAdr] & 0b01111111
        
        if (d == 0 ){
            return 0
        } else if (d == 0x7F) {
            return 99
        } else {
            return Int(d-0x1C)
            
        }
    }
    
    
    
    
    func levelGoesDownAtStep(_ step :Int) -> Bool{
        if (step == 1) {
            return false
        } else {
            let fromLevel = getDCA1LevelForStep(step-1)
            let toLevel = getDCA1LevelForStep(step)
            if (fromLevel <= toLevel){
                return false
            } else {
                return true
            }
        }
    }
    
    func levelGoesDown(step: Int,env: EnvName) -> Bool{
        
        if (step == 1) {
            return false
        }
        var fromLevel = 0
        var toLevel = 0
        
        switch (env) {
        case EnvName.DCA1:
            fromLevel = getDCA1LevelForStep(step-1)
            toLevel = getDCA1LevelForStep(step)
            break
        case EnvName.DCW1:
            fromLevel = getDCW1LevelForStep(step-1)
            toLevel = getDCW1LevelForStep(step)
            break
        }
        
        if (fromLevel <= toLevel){
            return false
        } else {
            return true
        }
        
    }
    
    func getDCA1RateForStep(_ step:Int) -> Int {
        let stepAdr = 0x15 + (step-1)*2
        var d = Double(data[stepAdr])
        
        if levelGoesDown(step: step, env: EnvName.DCA1){
            d = d-0x80
        }
        
        if (d == 0 ){
            return 0
        } else if (d == 0x77) {
            return 99
        } else {
            return Int(floor(d * 99)/0x77 + 1)
        }
    }
    
    func getDCW1EndStep() -> Int {
        let b = data[0x25] + 1
        return Int(b)
    }
    
    func getDCW1SustainStep() -> Int {
        var sustain = 0
        for step in 1...8 {
            let adr = 0x27 + (step-1)*2
            let sustainByte = data[adr] & 0x80
            if (sustainByte == 0x80){
                sustain = step
            }
        }
        return sustain
    }
    
    func getDCW1LevelForStep(_ step:Int) -> Int {
        let stepAdr = 0x27 + (step-1)*2
        let d = Double(data[stepAdr] & 0b01111111)
        
        if (d == 0 ){
            return 0
        } else if (d == 0x7F) {
            return 99
        } else {
            return Int(floor(d * 99)/0x7F + 1)
        }
    }
    
    func getDCW1RateForStep(_ step:Int) -> Int {
        let stepAdr = 0x26 + (step-1)*2
        var d = Double(data[stepAdr])
        
        if levelGoesDown(step: step, env: EnvName.DCW1){
            d = d-0x80
        }
        
        
        
        if (d == 8 ){
            return 0
        } else if (d == 0x7F) {
            return 99
        } else {
            return Int(floor((d-8) * 99)/119 + 1)
            
        }
    }
    
    func getDCO1EndStep() -> Int {
        let b = data[0x36] + 1
        return Int(b)
    }
    
    func getDCO1SustainStep() -> Int {
        var sustain = 0
        for step in 1...8 {
            let adr = 0x38 + (step-1)*2
            let sustainByte = data[adr] & 0x80
            if (sustainByte == 0x80){
                sustain = step
            }
        }
        return sustain
    }
    
    func getDCO1LevelForStep(_ step:Int) -> Int {
        return 0
    }
    
    func getDCO1RateForStep(_ step:Int) -> Int {
        return 0
    }
 */
    
}
